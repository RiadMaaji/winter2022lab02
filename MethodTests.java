public class MethodTests {
 public static void main (String[] args) {
  int x = 10;
  System.out.println(x);
  methodNoInputNoReturn();
  System.out.println(x);
  methodOneInputNoReturn(10);
  methodOneInputNoReturn(x);
  methodOneInputNoReturn(x + 50);
  methodTwoInputNoReturn(10, 10.5);
  int z = methodNoInputReturnInt();
  System.out.println(z);
  System.out.println(sumSquareRoot(6, 3));
  String s1 = "hello";
  String s2 = "goodbye";
  System.out.println(s1.length());
  System.out.println(s2.length());
  System.out.println(SecondClass.addOne(50));
  SecondClass sc = new SecondClass();
  System.out.println(sc.addTwo(50));
 }
 public static void methodNoInputNoReturn() {
  System.out.println("I'm in a method that takes no input and returns nothing");
  int x = 50;
  System.out.println(x);
 }
 public static void methodOneInputNoReturn(int value) {
  System.out.println("Inside the method one input no return");
 }
 public static void methodTwoInputNoReturn(int value1, double value2) {
   
 }
 public static int methodNoInputReturnInt() {
  return 6; 
 }
 public static double sumSquareRoot(int x, int y) {
  int z = x + y;
  return Math.sqrt(z);
 }
}
