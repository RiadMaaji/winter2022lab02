import java.util.Scanner;
public class PartThree {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        AreaComputations area = new AreaComputations();
        System.out.println("Enter the length of the square:");
        int sqLength = s.nextInt();
        System.out.println("Enter the length of the rectangle:");
        int recLength = s.nextInt();
        System.out.println("Enter the width of the square:"); 
        int recWidth = s.nextInt();
        System.out.println("The area of the square is : " + AreaComputations.areaSquare(sqLength));
        System.out.println("The area of the rectangle is : " + area.areaRectangle(recLength, recWidth));
    }
}